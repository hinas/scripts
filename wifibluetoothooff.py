import os
import time

def turnoff_wifi() :
    os.system("networksetup -setairportpower airport off")

def turnoff_bluetooth() :
    os.system("networksetup -setnetworkserviceenabled en3 off")


#after 15 minutes, turnoff wifi and bluetooth
time.sleep(900)
# turnoff_wifi()
turnoff_wifi()
